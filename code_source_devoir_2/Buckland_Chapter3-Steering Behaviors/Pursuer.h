#pragma once
#include "Vehicle.h"

class Pursuer : public Vehicle
{
public:
	Pursuer(
		GameWorld* world,
		Vector2D position,
		double    rotation,
		Vector2D velocity,
		double    mass,
		double    max_force,
		double    max_speed,
		double    max_turn_rate,
		double    scale,
		Vehicle* pursued,
		const Vector2D offset);

	void ChangeBehavior(Vehicle * pursued, const Vector2D offset);
	void UpdateOffset(const Vector2D offset);
};

