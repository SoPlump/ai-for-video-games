# Fonctionnalités ajoutées

**Mode file indienne** : les poursuiveurs forment une file indienne en suivant l'agent devant lui et en gardant une distance entre eux.
**Mode protecteur** : les poursuiveurs entourent le leader.

# Touche pour utiliser les fonctionnalités du programme

**B**: Basculer entre les modes "file indienne" et "protecteurs"
**N**: Activer ou désactiver le contrôle du *leader* par le joueur. Utiliser WASD pour se déplacer.

**Resources Files/params.ini**: Pour modifier le nombre d'agents, la vitesse du *leader* quand le joueur le contrôle.
