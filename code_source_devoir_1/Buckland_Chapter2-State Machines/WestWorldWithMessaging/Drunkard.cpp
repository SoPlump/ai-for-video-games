#include "Drunkard.h"

bool Drunkard::HandleMessage(const Telegram& msg)
{
	return m_pStateMachine->HandleMessage(msg);
}

void Drunkard::Update()
{
	//set text color to green
	SetTextColor(FOREGROUND_BLUE | FOREGROUND_INTENSITY);

	m_pStateMachine->Update();
}

void Drunkard::UpdateDrunkLevel(int value)
{
	m_iDrunkLevel += value;

	if (m_iDrunkLevel <= 0) m_iDrunkLevel = 0;
}

void Drunkard::UpdateMoney(int value)
{
	m_iMoney += value;

	if (m_iMoney < 0) m_iMoney = 0;
}

bool Drunkard::IsDrunk()
{
	if (m_iDrunkLevel >= m_iLucidityLimit) return true;
	return false;
}

bool Drunkard::IsLucid()
{
	if (m_iDrunkLevel <= 0) return true;
	return false;
}

bool Drunkard::HasMoney()
{
	if (m_iMoney > 0) return true;
	return false;
}
