#include "Pursuer.h"
#include "SteeringBehaviors.h"

Pursuer::Pursuer(
	GameWorld* world,
	Vector2D position,
	double    rotation,
	Vector2D velocity,
	double    mass,
	double    max_force,
	double    max_speed,
	double    max_turn_rate,
	double    scale,
	Vehicle* pursued,
	const Vector2D offset) 
	: Vehicle(
		world,
		position,
		rotation,
		velocity,
		mass,
		max_force,
		max_speed,
		max_turn_rate,
		scale)
{
	Vehicle::Steering()->OffsetPursuitOn(pursued, offset);
	Vehicle::Steering()->SeparationOn();
}

void Pursuer::ChangeBehavior(Vehicle* pursued, const Vector2D offset)
{
	Vehicle::Steering()->OffsetPursuitOff();
	Vehicle::Steering()->OffsetPursuitOn(pursued, offset);
}

void Pursuer::UpdateOffset(const Vector2D offset)
{
	Vehicle::Steering()->SetOffset(offset);
}
