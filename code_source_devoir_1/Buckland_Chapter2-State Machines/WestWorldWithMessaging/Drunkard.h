#pragma once

//------------------------------------------------------------------------
//
//  Name: MinersWife.h
//
//  Desc: class to implement Miner Bob's wife.
//
//  Author: Mat Buckland 2003 (fup@ai-junkie.com)
//
//------------------------------------------------------------------------

#include <string>

#include "fsm/State.h"
#include "BaseGameEntity.h"
#include "Locations.h"
#include "DrunkardOwnedStates.h"
#include "misc/ConsoleUtils.h"
#include "Miner.h"
#include "fsm/StateMachine.h"
#include "misc/Utils.h"



class Drunkard : public BaseGameEntity
{
private:

    //an instance of the state machine class
    StateMachine<Drunkard>* m_pStateMachine;

    location_type   m_Location;

    int m_iLucidityLimit;
    int m_iDrunkLevel;
    int m_iMoney;

public:

    Drunkard(int id) :
        m_Location(saloon),
        m_iLucidityLimit(5),
        m_iDrunkLevel(0),
        m_iMoney(2),
        BaseGameEntity(id)

    {
        //set up the state machine
        m_pStateMachine = new StateMachine<Drunkard>(this);

        m_pStateMachine->SetCurrentState(SteelMoney::Instance());
    }

    ~Drunkard() { delete m_pStateMachine; }


    //this must be implemented
    void          Update();

    //so must this
    virtual bool  HandleMessage(const Telegram& msg);

    StateMachine<Drunkard>* GetFSM()const { return m_pStateMachine; }

    //----------------------------------------------------accessors
    location_type Location()const { return m_Location; }
    void          ChangeLocation(location_type loc) { m_Location = loc; }

    void UpdateDrunkLevel(int value);
    void UpdateMoney(int value);
    bool IsDrunk();
    bool IsLucid();
    bool HasMoney();

};