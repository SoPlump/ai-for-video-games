#include "DrunkardOwnedStates.h"
#include "MinerOwnedStates.h"
#include "Drunkard.h"
#include "Locations.h"
#include "Time/CrudeTimer.h"
#include "MessageDispatcher.h"
#include "MessageTypes.h"
#include "EntityNames.h"

#include <iostream>
using std::cout;

#ifdef TEXTOUTPUT
#include <fstream>
extern std::ofstream os;
#define cout os
#endif

//-----------------------------------------------------------------------DrinkAlcool

DrinkAlcool* DrinkAlcool::Instance()
{
    static DrinkAlcool instance;

    return &instance;
}

void DrinkAlcool::Enter(Drunkard* drunkard)
{
    cout << "\n" << GetNameOfEntity(drunkard->ID()) << ": Give me all the alcool you have!";
}

void DrinkAlcool::Execute(Drunkard* drunkard)
{
    if (drunkard->IsDrunk())
    {
        drunkard->GetFSM()->ChangeState(Wasted::Instance());
    }

    if (!drunkard->HasMoney())
    {
        drunkard->GetFSM()->ChangeState(SteelMoney::Instance());
    }

    drunkard->UpdateDrunkLevel(1);
    drunkard->UpdateMoney(-1);
}

void DrinkAlcool::Exit(Drunkard* drunkard)
{
}

bool DrinkAlcool::OnMessage(Drunkard* drunkard, const Telegram& msg)
{
    SetTextColor(BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

    switch (msg.Msg)
    {
    case Msg_HeresBob:
    {
        cout << "\nMessage handled by " << GetNameOfEntity(drunkard->ID()) << " at time: "
            << Clock->GetCurrentTime();

        SetTextColor(FOREGROUND_GREEN | FOREGROUND_INTENSITY);

        cout << "\n" << GetNameOfEntity(drunkard->ID()) <<
            ": Not you again!";

        Dispatch->DispatchMessage(SEND_MSG_IMMEDIATELY,                  //time delay
            drunkard->ID(),           //sender ID
            ent_Miner_Bob,           //receiver ID
            Msg_OhNoNotYou,        //msg
            NO_ADDITIONAL_INFO);

        drunkard->GetFSM()->ChangeState(Fight::Instance());
    }

    return true;

    }//end switch

    return false;
}

//-------------------------------------------------------------------------Wasted

Wasted* Wasted::Instance()
{
    static Wasted instance;

    return &instance;
}


void Wasted::Enter(Drunkard* drunkard)
{
    cout << "\n" << GetNameOfEntity(drunkard->ID()) << ": I'm not feeling so good... Zzz...";
}


void Wasted::Execute(Drunkard* drunkard)
{
    drunkard->UpdateDrunkLevel(-1);

    if (drunkard->IsLucid())
    {
        if (drunkard->HasMoney())
        {
            drunkard->GetFSM()->ChangeState(DrinkAlcool::Instance());
        }
        else
        {
            drunkard->GetFSM()->ChangeState(SteelMoney::Instance());
        }
    }
}

void Wasted::Exit(Drunkard* drunkard)
{
}

bool Wasted::OnMessage(Drunkard* drunkard, const Telegram& msg)
{
    return false;
}

//------------------------------------------------------------------------SteelMoney

SteelMoney* SteelMoney::Instance()
{
    static SteelMoney instance;

    return &instance;
}


void SteelMoney::Enter(Drunkard* drunkard)
{
    cout << "\n" << GetNameOfEntity(drunkard->ID()) << ": Here's some money in da pocket";
}


void SteelMoney::Execute(Drunkard* drunkard)
{
    drunkard->UpdateMoney(5);
    drunkard->GetFSM()->ChangeState(DrinkAlcool::Instance());
}

void SteelMoney::Exit(Drunkard* drunkard)
{
}


bool SteelMoney::OnMessage(Drunkard* drunkard, const Telegram& msg)
{
    return false;
}


//------------------------------------------------------------------------Fight

Fight* Fight::Instance()
{
    static Fight instance;

    return &instance;
}


void Fight::Enter(Drunkard* drunkard)
{
    cout << "\n" << GetNameOfEntity(drunkard->ID()) << ": Gonna smack your jaw!";
}


void Fight::Execute(Drunkard* drunkard)
{
    drunkard->GetFSM()->ChangeState(Wasted::Instance());
}

void Fight::Exit(Drunkard* drunkard)
{
    SetTextColor(FOREGROUND_BLUE | FOREGROUND_INTENSITY);

    cout << "\n" << GetNameOfEntity(drunkard->ID()) << ": Ouch";
}


bool Fight::OnMessage(Drunkard* drunkard, const Telegram& msg)
{
    return false;
}