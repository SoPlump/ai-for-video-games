#pragma once
#include "fsm/State.h"

class Drunkard;


//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class DrinkAlcool : public State<Drunkard>
{
private:

	DrinkAlcool() {}

	//copy ctor and assignment should be private
	DrinkAlcool(const DrinkAlcool&);
	DrinkAlcool& operator=(const DrinkAlcool&);

public:

	//this is a singleton
	static DrinkAlcool* Instance();

	virtual void Enter(Drunkard* drunkard);

	virtual void Execute(Drunkard* drunkard);

	virtual void Exit(Drunkard* drunkard);

	virtual bool OnMessage(Drunkard* drunkard, const Telegram& msg);

};


//------------------------------------------------------------------------
//

//------------------------------------------------------------------------

class Wasted : public State<Drunkard>
{
private:

	Wasted() {}

	//copy ctor and assignment should be private
	Wasted(const Wasted&);
	Wasted& operator=(const Wasted&);

public:

	//this is a singleton
	static Wasted* Instance();

	virtual void Enter(Drunkard* drunkard);

	virtual void Execute(Drunkard* drunkard);

	virtual void Exit(Drunkard* drunkard);

	virtual bool OnMessage(Drunkard* agent, const Telegram& msg);
};


//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class SteelMoney : public State<Drunkard>
{
private:

	SteelMoney() {}

	//copy ctor and assignment should be private
	SteelMoney(const SteelMoney&);
	SteelMoney& operator=(const SteelMoney&);

public:

	//this is a singleton
	static SteelMoney* Instance();

	virtual void Enter(Drunkard* drunkard);

	virtual void Execute(Drunkard* drunkard);

	virtual void Exit(Drunkard* drunkard);

	virtual bool OnMessage(Drunkard* drunkard, const Telegram& msg);

};


//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class Fight : public State<Drunkard>
{
private:

	Fight() {}

	//copy ctor and assignment should be private
	Fight(const Fight&);
	Fight& operator=(const Fight&);

public:

	//this is a singleton
	static Fight* Instance();

	virtual void Enter(Drunkard* drunkard);

	virtual void Execute(Drunkard* drunkard);

	virtual void Exit(Drunkard* drunkard);

	virtual bool OnMessage(Drunkard* drunkard, const Telegram& msg);
};